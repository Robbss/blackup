﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace BlackUpService
{
    [RunInstaller(true)]
    public partial class BlackUpServiceInstaller : System.Configuration.Install.Installer
    {
        public BlackUpServiceInstaller()
        {
            InitializeComponent();
        }
    }
}
