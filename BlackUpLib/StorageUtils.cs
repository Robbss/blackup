﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackUpLib
{
    public static class StorageUtils
    {
        public static string AuthorizationToken { get; set; }
        public static string Username { get; set; }
        public static string Hash { get; set; }
    }
}
