﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BlackUpLib
{
    [Serializable]
    public class FileInformation
    {
        #region Fields

        private string name;
        private DateTime date;

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
        }

        public DateTime Date
        {
            get
            {
                return date;
            }
        }

        #endregion

        #region Constructors

        public FileInformation(string filename)
        {
            int length = filename.Length;
            int datelength = 12;

            string datestring = filename.Substring(length - datelength, datelength);

            int day = int.Parse(datestring.Substring(0, 2));
            int month = int.Parse(datestring.Substring(2, 2));
            int year = int.Parse(datestring.Substring(4, 2));

            int hour = int.Parse(datestring.Substring(6, 2));
            int minute = int.Parse(datestring.Substring(8, 2));
            int seconds = int.Parse(datestring.Substring(10, 2));

            this.date = new DateTime(year, month, day, hour, minute, seconds);
            int len = (length - datelength - 1);
            this.name = filename.Substring(0, len);
        }

        #endregion

        #region Methods
        #endregion
    }
}
