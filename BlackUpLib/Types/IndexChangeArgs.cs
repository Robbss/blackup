﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackUpLib
{
    public class IndexChangeArgs
    {
        #region Fields

        private int index;

        #endregion

        #region Properties

        public int Index
        {
            get
            {
                return index;
            }
        }

        #endregion

        #region Constructors

        public IndexChangeArgs(int index)
        {
            this.index = index;
        }

        #endregion

        #region Methods
        #endregion
    }
}
