﻿namespace BlackUpLib.Interfaces
{
    public interface IChange
    {
        string Path { get; }

        void Sync(BlackUpLib.Netcode.Client cl);
    }
}
