﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using BlackUpLib.Netcode;
using System.Net.Sockets;

namespace BlackUpLib
{
    public interface IMessageHandler
    {
        void Handle(Socket socket, Package package);
    }
}
