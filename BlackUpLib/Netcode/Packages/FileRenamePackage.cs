﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackUpLib.Netcode
{
    class FileRenamePackage : Package
    {
        public string OldPath { get; private set; }
        public string NewPath { get; private set; }

        public FileRenamePackage(BlackUpLib.Environmenties.RenameChange rnm)
            : this(rnm.Path, rnm.NewPath)
        { }

        public FileRenamePackage(string OldPath, string NewPath)
            : base(null)
        {
            this.OldPath = OldPath;
            this.NewPath = NewPath;
        }
    }
}
