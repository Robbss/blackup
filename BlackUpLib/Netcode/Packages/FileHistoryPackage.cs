﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackUpLib.Netcode
{
    [Serializable]
    public class FileHistoryPackage : Package
    {
        #region Fields

        private List<FileInformation> fileInformation;

        #endregion

        #region Properties

        public List<FileInformation> FileInformation
        {
            get
            {
                return fileInformation;
            }
            set
            {
                fileInformation = value;
            }
        }

        #endregion

        #region Constructors

        public FileHistoryPackage() : base(null)
        {
            this.FileInformation = new List<FileInformation>();
        }

        public FileHistoryPackage(List<FileInformation> fileInformation)
        {
            this.FileInformation = fileInformation;
        }

        #endregion

        #region Methods
        #endregion
    }
}
