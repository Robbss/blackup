﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackUpLib.Netcode
{
    [Serializable]
    public class ChunkDataPackage : Package
    {
        #region Fields

        private int chunkIndex;
        private byte[] chunkData;

        #endregion

        #region Properties

        public int ChunkIndex
        {
            get
            {
                return chunkIndex;
            }
            set
            {
                chunkIndex = value;
            }
        }

        public byte[] ChunkData
        {
            get
            {
                return chunkData;
            }
            set
            {
                chunkData = value;
            }
        }

        #endregion

        #region Constructors

        public ChunkDataPackage(string token, int chunkIndex, byte[] chunkData) : base(token)
        {
            this.chunkIndex = chunkIndex;
            this.chunkData = chunkData;
        }

        #endregion

        #region Methods

        public override int GetHashCode()
        {
            return chunkIndex.GetHashCode() + chunkData.Length.GetHashCode();
        }

        #endregion
    }
}
