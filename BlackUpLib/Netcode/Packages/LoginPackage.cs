﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackUpLib.Netcode
{
    [Serializable]
    public class LoginPackage : Package
    {
        #region Fields

        private string username;
        private string hash;

        #endregion

        #region Properties

        public string Username
        {
            get
            {
                return username;
            }
            set
            {
                username = value;
            }
        }

        public string Hash
        {
            get
            {
                return hash;
            }
            set
            {
                hash = value;
            }
        }

        #endregion

        #region Constructors

        public LoginPackage(string username, string hash) : base(null)
        {
            this.username = username;
            this.hash = hash;
        }

        #endregion

        #region Methods

        public override int GetHashCode()
        {
            return username.GetHashCode() + hash.GetHashCode();
        }

        #endregion
    }
}
