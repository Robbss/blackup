﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackUpLib.Netcode
{
    [Serializable]
    public class CreateAccountPackage : Package
    {
        #region Fields

        private string username;
        private string password;
        private string email;

        #endregion

        #region Properties

        public string Username
        {
            get
            {
                return username;
            }
            set
            {
                username = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        #endregion

        #region Constructors

        public CreateAccountPackage(string username, string password, string email)
        {
            this.username = username;
            this.password = password;
            this.email = email;
        }

        #endregion

        #region Methods
        #endregion
    }
}
