﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackUpLib.Netcode
{
    [Serializable]
    public class Package
    {
        #region Fields

        private string token;

        #endregion

        #region Properties

        public string Token
        {
            get
            {
                return token;
            }
            set
            {
                token = value;
            }
        }

        #endregion

        #region Constructors

        public Package()
        {
            token = null;
        }

        public Package(string token)
        {
            this.token = token;
        }

        #endregion

        #region Methods

        public override bool Equals(object obj)
        {
            Package pack = obj as Package;

            if (pack == null)
            {
                return false;
            }

            return GetHashCode() == pack.GetHashCode();
        }

        public override int GetHashCode()
        {
            return token.GetHashCode();
        }

        #endregion
    }
}