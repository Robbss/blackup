﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackUpLib.Netcode
{
    [Serializable]
    public class RequestDataPackage : Package
    {
        #region Fields

        private RequestDataType requestDataType;
        private string[] requestInformation;

        #endregion

        #region Properties

        public RequestDataType RequestDataType
        {
            get
            {
                return requestDataType;
            }
            set
            {
                requestDataType = value;
            }
        }

        public string[] RequestInformation
        {
            get
            {
                return requestInformation;
            }
            set
            {
                requestInformation = value;
            }
        }

        #endregion

        #region Constructors

        public RequestDataPackage(string token, RequestDataType requestDataType, params string[] requestInformation) : base(token)
        {
            this.requestDataType = requestDataType;
            this.requestInformation = requestInformation;
        }

        #endregion

        #region Methods
        #endregion
    }
}
