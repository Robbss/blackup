﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackUpLib.Netcode
{
    [Serializable]
    public class ChunkStartPackage : Package
    {
        #region Fields

        private string fileName;
        private string filePath;

        private int chunkCount;
        private int chunkCurrent;

        #endregion

        #region Properties

        public string FileName
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public string FilePath
        {
            get
            {
                return filePath;
            }

            set
            {
                filePath = value;
            }
        }

        public int ChunkCount
        {
            get
            {
                return chunkCount;
            }
            set
            {
                chunkCount = value;
            }
        }

        public int ChunkCurrent
        {
            get
            {
                return chunkCurrent;
            }
            set
            {
                chunkCurrent = value;
            }
        }

        #endregion

        #region Constructors

        public ChunkStartPackage(string token, string fileName, string filePath, int chunkCount, int chunkCurrent) : base(token)
        {
            this.fileName = fileName;
            this.filePath = filePath;
            this.chunkCount = chunkCount;
            this.chunkCurrent = chunkCurrent;
        }

        #endregion

        #region Methods
        #endregion
    }
}
