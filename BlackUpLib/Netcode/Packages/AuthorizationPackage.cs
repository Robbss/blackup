﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackUpLib.Netcode
{
    [Serializable]
    public class AuthorizationPackage : Package
    {
        #region Fields

        private AuthorizationState authorizationState;
        private Token authorizationToken;

        #endregion

        #region Properties

        public AuthorizationState AuthorizationState
        {
            get
            {
                return authorizationState;
            }
            set
            {
                authorizationState = value;
            }
        }

        public Token AuthorizationToken
        {
            get
            {
                return authorizationToken;
            }
            set
            {
                authorizationToken = value;
            }
        }

        #endregion

        #region Constructors

        public AuthorizationPackage(AuthorizationState authorizationState, Token authorizationToken) : base(null)
        {
            this.authorizationState = authorizationState;
            this.AuthorizationToken = authorizationToken;
        }

        #endregion

        #region Methods
        #endregion
    }
}
