﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BlackUpLib.Netcode
{
    public class Client
    {
        #region Fields

        private IPAddress address;
        private int port;

        private Socket socket;

        #endregion

        #region Properties

        #endregion

        #region Constructors

        public Client(IPAddress address, int port)
        {
            this.address = address;
            this.port = port;

            this.socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        }

        #endregion

        #region Methods

        public void Connect()
        {
            socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(address, port);
        }

        public void Close()
        {
            socket.Close();
        }

        public void Reconnect()
        {
            Close();
            Connect();
        }

        public void SendPackage(Package package) => Connection.SendPackage(socket, package);

        public Package ReadPackage() => Connection.ReadPackage(socket);

        public T ReadPackage<T>() where T : Package => Connection.ReadPackage<T>(socket);

        #endregion
    }
}
