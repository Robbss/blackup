﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BlackUpLib.Netcode
{
    public class ChunkSender
    {
        #region Fields

        private Client client;
        private Token token;

        private string path;
        private bool stop;

        private event EventHandler<IndexChangeArgs> onIndexChange;
        private event EventHandler onComplete;

        #endregion

        #region Properties

        public EventHandler<IndexChangeArgs> OnIndexChange
        {
            get
            {
                return onIndexChange;
            }
            set
            {
                onIndexChange = value;
            }
        }

        public EventHandler OnComplete
        {
            get
            {
                return onComplete;
            }
            set
            {
                onComplete = value;
            }
        }

        #endregion

        #region Constructors

        public ChunkSender(Client client, Token token, string path)
        {
            this.client = client;
            this.token = token;
            this.path = path;
        }

        #endregion

        #region Methods

        public int SendFile(int bufferSize, int index = 0)
        {
            FileInfo fileInfo = new FileInfo(path);

            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Open))
                {
                    byte[] buffer = new byte[bufferSize];
                    double totalCount = (double)fs.Length / (double)buffer.Length;

                    int chunkCount = (int)Math.Ceiling(totalCount);

                    ChunkStartPackage startPack = new ChunkStartPackage(token.Hash, fileInfo.Name, path, chunkCount, index);
                    client.SendPackage(startPack);

                    AuthorizationPackage authorization = client.ReadPackage<AuthorizationPackage>();

                    if (authorization.AuthorizationState != AuthorizationState.Authorized)
                    {
                        throw new Exception("Incorrect authorization");
                    }

                    fs.Position = bufferSize * index;

                    while (fs.Position < fs.Length)
                    {
                        long count = fs.Length - fs.Position < buffer.Length ? fs.Length - fs.Position : buffer.Length;

                        if (count < buffer.Length)
                        {
                            buffer = new byte[count];
                        }

                        fs.Read(buffer, 0, (int)count);

                        ChunkDataPackage dataPack = new ChunkDataPackage(token.Hash, index, buffer);
                        client.SendPackage(dataPack);

                        index++;
                        OnIndexChange(this, new IndexChangeArgs(index));

                        if (stop)
                        {
                            client.SendPackage(new Package());

                            return index;
                        }
                    }
                }
            }
            catch { }

            OnComplete(this, new EventArgs());

            return -1;
        }

        public void Stop()
        {
            stop = true;
        }

        #endregion
    }
}
