﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackUpLib.Netcode
{
    [Serializable]
    public class Token
    {
        #region Fields

        private string hash;
        private DateTime expires;

        #endregion

        #region Properties

        public string Hash
        {
            get
            {
                return hash;
            }
            set
            {
                hash = value;
            }
        }

        public DateTime Expires
        {
            get
            {
                return expires;
            }
            set
            {
                expires = value;
            }
        }

        #endregion

        #region Constructors

        public Token()
        {
            this.hash = null;
        }

        public Token(string hash, DateTime expires)
        {
            this.hash = hash;
            this.expires = expires;
        }

        #endregion

        #region Methods

        public static Token Create(string username, int lifeInMinutes)
        {
            string hash = CalculateHash(username, DateTime.Now);
            DateTime expireDate = DateTime.Now + new TimeSpan(0, lifeInMinutes, 0);

            return new Token(hash, expireDate);
        }

        private static string CalculateHash(string username, DateTime dateTime)
        {
            string complete = $"{username}{dateTime.ToString("MMddyyyy hh:mm:ss")}";
            byte[] bytes = Encoding.UTF8.GetBytes(complete);

            return Convert.ToBase64String(bytes);
        }

        #endregion
    }
}
