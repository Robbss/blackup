﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace BlackUpLib.Netcode
{
    public class Connection
    {
        #region Fields

        private Socket socket;
        private const int MaxSize = 1024 * 1024 * 50;

        #endregion

        #region Properties
        #endregion

        #region Constructors

        public Connection(Socket socket)
        {
            this.socket = socket;
        }

        #endregion

        #region Methods

        private void ListenOne()
        {
            //handler.Handle(socket, ReadPackage());
        }

        private void Listen()
        {
            while (socket.Connected)
            {
                ListenOne();
            }
        }

        public Package ReadPackage()
        {
            return ReadPackage(socket);
        }

        public static Package ReadPackage(Socket socket)
        {
            byte[] lengthBytes = new byte[4];
            socket.Receive(lengthBytes, 0, 4, SocketFlags.None);

            int length = BitConverter.ToInt32(lengthBytes, 0);

            if (length > MaxSize || length < 0)
            {
                return ReadPackage(socket);
            }

            byte[] headerBytes = new byte[length];
            int headerReceived = 0;

            while (headerReceived < length)
            {
                headerReceived += socket.Receive(headerBytes, headerReceived, length - headerReceived, SocketFlags.None);
            }

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            Package header;

            try
            {
                using (MemoryStream ms = new MemoryStream(headerBytes))
                {
                    header = (Package)binaryFormatter.Deserialize(ms);
                }
            }
            catch
            {
                header = null;
            }

            return header;
        }

        public static T ReadPackage<T>(Socket socket) where T : Package
        {
            return ReadPackage(socket) as T;
        }

        public static void SendPackage(Socket socket, Package package)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            byte[] headerBytes;

            using (MemoryStream ms = new MemoryStream())
            {
                formatter.Serialize(ms, package);
                headerBytes = ms.ToArray();
            }

            byte[] lengthBytes = BitConverter.GetBytes(headerBytes.Length);
            byte[] datas = lengthBytes.Concat(headerBytes).ToArray();

            socket.Send(datas, 0, datas.Length, SocketFlags.None);
        }

        public static Package Handle(Socket socket)
        {
            Connection connection = new Connection(socket);
            return connection.ReadPackage();
        }

        #endregion
    }
}
