﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;

namespace BlackUpLib.Netcode
{
    public class ThrottledStream : Stream
    {
        #region Fields

        private Stream stream;

        private long bytesPerSecond;
        private long byteCount;

        private long startTime;

        #endregion

        #region Properties

        public Stream Stream
        {
            get
            {
                return stream;
            }
        }

        public long BytesPerSecond
        {
            get
            {
                return bytesPerSecond;
            }
            set
            {
                bytesPerSecond = value;
            }
        }

        private long CurrentTime
        {
            get
            {
                return Environment.TickCount;
            }
        }

        public override bool CanRead
        {
            get
            {
                return stream.CanRead;
            }
        }

        public override bool CanSeek
        {
            get
            {
                return stream.CanSeek;
            }
        }

        public override bool CanWrite
        {
            get
            {
                return stream.CanWrite;
            }
        }

        public override long Length
        {
            get
            {
                return stream.Length;
            }
        }

        public override long Position
        {
            get
            {
                return stream.Position;
            }
            set
            {
                stream.Position = value;
            }
        }

        #endregion

        #region Constructors

        public ThrottledStream(Stream stream) : this(stream, 0)
        {

        }

        public ThrottledStream(Stream stream, long bytesPerSecond)
        {
            this.stream = stream;
            this.bytesPerSecond = bytesPerSecond;

            this.startTime = CurrentTime;
            this.byteCount = 0;
        }

        #endregion

        #region Methods

        public override void Flush()
        {
            stream.Flush();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return stream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            stream.SetLength(value);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            Throttle(count);

            return stream.Read(buffer, offset, count);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            Throttle(count);

            stream.Write(buffer, offset, count);
        }

        private void Throttle(long count)
        {
            if (bytesPerSecond <= 0 || count <= 0)
            {
                return;
            }

            byteCount += count;
            long elapsed = CurrentTime - startTime;

            if (elapsed > 0)
            {
                long bps = byteCount * 1000L / elapsed;

                if (bps > bytesPerSecond)
                {
                    long wake = byteCount * 1000L / bytesPerSecond;
                    int sleep = (int)(wake - elapsed);

                    if (sleep > 1)
                    {
                        Thread.Sleep(sleep);
                        Reset();
                    }
                }
            }
        }

        private void Reset()
        {
            long difference = CurrentTime - startTime;

            if (difference > 1000)
            {
                byteCount = 0;
                startTime = CurrentTime;
            }
        }

        #endregion
    }
}
