﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace BlackUpLib.Netcode
{
    public class Server
    {
        #region Fields

        private IPAddress address = IPAddress.Any;
        private int port = 25560;

        private TcpListener tcpListener;
        private Dictionary<Type, List<IMessageHandler>> handlers;

        private Thread listenThread;
        private bool isListening;

        #endregion

        #region Properties

        public Dictionary<Type, List<IMessageHandler>> Handlers
        {
            get
            {
                return handlers;
            }
        }

        public bool IsListening
        {
            get
            {
                return isListening;
            }
        }

        #endregion

        #region Constructors

        public Server(int port)
        {
            this.port = port;
            this.tcpListener = new TcpListener(address, port);
            this.handlers = new Dictionary<Type, List<IMessageHandler>>();
        }

        #endregion

        #region Methods

        public Thread Start()
        {
            tcpListener.Start();
            isListening = true;

            listenThread = new Thread(Listen);
            listenThread.Start();

            return listenThread;
        }

        public void StartSync()
        {
            Start().Join();
        }

        public void Stop()
        {
            isListening = false;
            listenThread.Join();
        }

        public void Register(Type type, IMessageHandler handler)
        {
            if (!handlers.ContainsKey(type))
            {
                handlers.Add(type, new List<IMessageHandler>());
            }

            handlers[type].Add(handler);
        }

        public void Unregister(Type type)
        {
            if (handlers.ContainsKey(type))
            {
                handlers.Remove(type);
            }
        }

        private void Listen()
        {
            while (IsListening)
            {
                Socket socket = tcpListener.AcceptSocket();

                Thread thread = new Thread(() =>
                {
                    Package package = Connection.ReadPackage(socket);

                    if (package != null)
                    {
                        Type packageType = package.GetType();

                        if (handlers.ContainsKey(packageType))
                        {
                            foreach (IMessageHandler handler in handlers[packageType])
                            {
                                handler.Handle(socket, package);
                            }
                        }
                    }
                });

                thread.Start();
            }
        }

        #endregion
    }
}
