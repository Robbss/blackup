﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace BlackUpLib.Security
{
    public static class HashHelper
    {
        #region Fields

        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Methods

        public static string GenerateHash(string password)
        {
            StringBuilder sb = new StringBuilder();

            using (MD5 md5 = MD5.Create())
            {
                byte[] bytes = Encoding.UTF8.GetBytes(password);
                byte[] hashbytes = md5.ComputeHash(bytes);

                for (int i = 0; i < hashbytes.Length; i++)
                {
                    sb.Append(hashbytes[i].ToString("X2"));
                }
            }

            return sb.ToString();
        }

        public static byte[] GenerateHash(byte[] bytes)
        {
            byte[] hashbytes;

            using (MD5 md5 = MD5.Create())
            {
                hashbytes = md5.ComputeHash(bytes);
            }

            return hashbytes;
        }

        #endregion
    }
}
