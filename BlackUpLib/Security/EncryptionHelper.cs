﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security;
using System.Security.Cryptography;
using System.IO;

namespace BlackUpLib.Security
{
    public class EncryptionHelper
    {
        #region Fields

        private const string initial = "blackup-iv blackup-iv blackup-iv blackup-iv blackup-iv blackup-iv";
        private const byte ByteBits = 0x8;

        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Methods

        private Stream EncDecStream(Stream stream, byte[] password, bool decrypt)
        {
            password = HashHelper.GenerateHash(password);
            Stream memoStream = new MemoryStream((int)stream.Length);

            RijndaelManaged rm = new RijndaelManaged
            {
                KeySize = password.Length * ByteBits,
                Key = password,
            };
            rm.IV = Encoding.UTF8.GetBytes(initial.Remove(rm.BlockSize / ByteBits, initial.Length - (rm.BlockSize / ByteBits)));
            rm.Padding = PaddingMode.Zeros;

            var cryptoStream = new CryptoStream(decrypt ? stream : memoStream,
                decrypt ? rm.CreateDecryptor() : rm.CreateEncryptor(),
                decrypt ? CryptoStreamMode.Read : CryptoStreamMode.Write);

            if (!decrypt)
            {
                for (int i = 0; i < stream.Length; i++)
                    cryptoStream.WriteByte((byte)stream.ReadByte());

                cryptoStream.FlushFinalBlock();
            }
            else
            {
                cryptoStream.CopyTo(memoStream);
            }
            
            memoStream.Position = 0;
            return memoStream;
        }

        /// <summary>
        /// Encrypts a stream
        /// </summary>
        /// <param name="stream">the stream to encrypt</param>
        /// <param name="password">The password to encrypt with, NOTE: This has to be between 16 and 32 bytes long</param>
        /// <returns>The encrypted stream.</returns>
        public Stream EncryptStream(Stream stream, byte[] password)
            => EncDecStream(stream, password, false);

        /// <summary>
        /// Encrypts a stream
        /// </summary>
        /// <param name="stream">the stream to encrypt</param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Stream EncryptStream(Stream stream, string password)
            => EncDecStream(stream, Encoding.UTF8.GetBytes(password), false);

        public Stream DecryptStream(Stream stream, byte[] password)
            => EncDecStream(stream, password, true);

        public Stream DecryptStream(Stream stream, string password)
            => EncDecStream(stream, Encoding.UTF8.GetBytes(password), true);

        #endregion
    }
}
