﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using BlackUpLib.Netcode;
using BlackUpLib.Interfaces;

namespace BlackUpLib.Environmenties
{
    /// <summary>
    /// Handles a single stage between every sync with the server
    /// </summary>
    public class Stage
    {
        public List<IChange> changes { get; private set; }

        public Stage()
            : this(new IChange[0])
        { }

        public Stage(params IChange[] stagedItems)
        {
            changes = new List<IChange>();
            changes.AddRange(stagedItems);

            FileWatcherManager.Instance.Subscribe(RenameEvent);
            FileWatcherManager.Instance.Subscribe(ChangeEvent);
        }

        /// <summary>
        /// Adds a change to the list of changes associated with this stage
        /// </summary>
        /// <param name="item">The change to add</param>
        public void Add(IChange item)
            => changes.Add(item);

        /// <summary>
        /// Synchronizes the change with the Server
        /// </summary>
        public Change[] FilesToSync()
        {
            List<Change> c = (from ch in changes where ch is Change select (ch as Change?).Value).ToList();

            for(int i = 0; i < c.Count - 1; i++)
                for(int j = i + 1; j < c.Count; j++)
                    while (j != c.Count && c[i].Path == c[j].Path)
                        c.RemoveAt(j);
            
            changes.Clear();
            return c.ToArray();
        }

        /// <summary>
        /// Handles a renameevent
        /// </summary>
        /// <param name="path">the path of the old file</param>
        /// <param name="newPath">the path of the new file</param>
        /// <param name="args">the arguments of the rename event</param>
        /// <param name="type">The type of event happening</param>
        private void RenameEvent(string path, string newPath, RenamedEventArgs args, FileEventType type)
            => changes.Add(new RenameChange(path, newPath));

        /// <summary>
        /// Handles an ordinary event
        /// </summary>
        /// <param name="path">The path to the file with the change</param>
        /// <param name="args">The arguments to use with the file</param>
        /// <param name="type">The file event type.</param>
        private void ChangeEvent(string path, FileSystemEventArgs args, FileEventType type)
        {
            if (type == FileEventType.Change || type == FileEventType.Create)
                changes.Add(new Change(path));
        }
    }
}
