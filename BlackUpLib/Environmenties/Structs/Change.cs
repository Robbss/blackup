﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using BlackUpLib.Interfaces;
using BlackUpLib.Netcode;
using BlackUpLib.Security;

namespace BlackUpLib.Environmenties
{
    public struct Change : IChange
    {
        public string Path { get; private set; }

        public Change(string path)
        {
            this.Path = path;
        }

        public void Sync(Client client)
        {
            ChunkSender cs = new ChunkSender(client, null, Path);
            cs.SendFile(1024 * 1024 * 10);
        }
    }
}
