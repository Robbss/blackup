﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackUpLib.Environmenties
{
    public struct TempFile
    { 
        public string Sender { get; private set; }
        public string Header { get; private set; }
        public string[] Args { get; private set; }

        public TempFile(string Sender, string Header, params string[] args)
        {
            this.Sender = Sender;
            this.Header = Header;
            this.Args = args;
        }
    }
}
