﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackUpLib.Interfaces;
using BlackUpLib.Netcode;

namespace BlackUpLib.Environmenties
{ 
    public struct RenameChange : IChange
    {
        public string Path { get; private set; }
        public string NewPath { get; private set; }

        public RenameChange(string path, string NewName)
        {
            this.Path = path;
            this.NewPath = NewName;
        }

        public void Sync(Client client)
        {
            Netcode.FileRenamePackage frp = new Netcode.FileRenamePackage(this);
            client.SendPackage(frp);
        }
    }
}
