﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace BlackUpLib.Environmenties
{
    public class TempWrangler
    {
        #region Fields
        private static TempWrangler _instance;
        private FileSystemWatcher fsw;

        private List<Action<TempFile>> subscribers;
        #endregion

        #region Properties
        public static TempWrangler Instance { get { return _instance == null ? _instance = new TempWrangler() : _instance; } }
        #endregion

        #region Constructors
        private TempWrangler()
        {
            subscribers = new List<Action<TempFile>>();

            if (!Directory.Exists("Temp"))
                Directory.CreateDirectory("Temp");

            fsw = new FileSystemWatcher("Temp");

            fsw.Created += FswCreateHandle;
            fsw.Changed += FswChangeHandle;
            fsw.Error += FswErrorHandle;

            fsw.BeginInit();
        }
        #endregion

        #region Methods
        private void FswCreateHandle(object sender, FileSystemEventArgs args)
        {
            TriggerEvent(args.FullPath);
            File.Delete(args.FullPath);
        }

        private void FswChangeHandle(object sender, FileSystemEventArgs args)
        {
            TriggerEvent(args.FullPath);
            File.Delete(args.FullPath);
        }

        private void FswErrorHandle(object sender, ErrorEventArgs args)
        {
            var o = sender as FileSystemEventArgs;

            fsw = new FileSystemWatcher(o.FullPath);

            fsw.Created += FswCreateHandle;
            fsw.Changed += FswChangeHandle;
            fsw.Error += FswErrorHandle;

            fsw.BeginInit();
        }

        private void TriggerEvent(string path)
        {
            string[] sTemp = new StreamReader(new FileStream(path, FileMode.Open)).ReadToEnd().Split('|');
            TempFile tf = new TempFile(sTemp[0], sTemp[1], sTemp[2].Split(':'));

            for (int i = 0; i < subscribers.Count; i++)
                subscribers[i](tf);
        }

        public void Subscribe(Action<TempFile> a)
            => subscribers.Add(a);

        public void Unsubscribe(Action<TempFile> a)
            => subscribers.Remove(a);

        #endregion
    }
}
