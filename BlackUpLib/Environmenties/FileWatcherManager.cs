﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace BlackUpLib.Environmenties
{
    public enum FileEventType { Change, Rename, Delete, Create }

    public class FileWatcherManager
    {
        private static FileWatcherManager _instance;
        public static FileWatcherManager Instance { get { return _instance == null ? _instance = new FileWatcherManager() : _instance; } }

        public string[] RootDirectories { get { return panopticon.Keys.ToArray(); } }

        private Dictionary<string, FileSystemWatcher> panopticon;
        private List<Action<string, FileSystemEventArgs, FileEventType>> eventSubscribers;
        private List<Action<string, string, RenamedEventArgs, FileEventType>> renameSubscribers;

        private FileWatcherManager()
        {
            panopticon = new Dictionary<string, FileSystemWatcher>();
            eventSubscribers = new List<Action<string, FileSystemEventArgs, FileEventType>>();
            renameSubscribers = new List<Action<string, string, RenamedEventArgs, FileEventType>>();

            Subscribe(FolderDeletion);
            TempWrangler.Instance.Subscribe(OnFolderAddition);
        }

        public void AddFolderToWatch(string path)
            => AddFolderToWatch(path, true);

        public void AddFolderToWatch(string path, bool entireTree)
        {
            if (!panopticon.ContainsKey(path))
            {
                FileSystemWatcher fsw = new FileSystemWatcher(path);
                panopticon.Add(path, fsw);
                fsw.Changed += new FileSystemEventHandler((o, fsea) => RaiseEvent(fsea.FullPath, fsea, FileEventType.Change));
                fsw.Created += new FileSystemEventHandler((o, fsea) => RaiseEvent(fsea.FullPath, fsea, FileEventType.Create));
                fsw.Deleted += new FileSystemEventHandler((o, fsea) => RaiseEvent(fsea.FullPath, fsea, FileEventType.Delete));
                fsw.Renamed += new RenamedEventHandler((o, rea) => RaiseEvent(rea.FullPath, rea.OldFullPath, rea, FileEventType.Rename));
                fsw.Error += new ErrorEventHandler((o, eea) => FswErrorHandle(o as FileSystemWatcher));
                fsw.EnableRaisingEvents = true;

                if (entireTree)
                    fsw.IncludeSubdirectories = true;
            }
        }

        public void Subscribe(Action<string, string, RenamedEventArgs, FileEventType> subscriber)
            => renameSubscribers.Add(subscriber);

        public void Unsubscrie(Action<string, string, RenamedEventArgs, FileEventType> subscriber)
            => renameSubscribers.Remove(subscriber);

        public void Subscribe(Action<string, FileSystemEventArgs, FileEventType> subscriber)
            => eventSubscribers.Add(subscriber);

        public void Unsubscribe(Action<string, FileSystemEventArgs, FileEventType> subscriber)
            => eventSubscribers.Remove(subscriber);

        public void RaiseEvent(string path, FileSystemEventArgs fsea, FileEventType type)
        {
            foreach (var a in eventSubscribers)
                a(path, fsea, type);
        }

        public void RaiseEvent(string path, string newPath, RenamedEventArgs rea, FileEventType type)
        {
            foreach (var a in renameSubscribers)
                a(path, newPath, rea, type);
        }

        private void FswErrorHandle(FileSystemWatcher fsw)
        {
            var nsfw = new FileSystemWatcher(fsw.Path);
            panopticon[fsw.Path] = nsfw;
            nsfw.Renamed += new RenamedEventHandler((o, rea) => RaiseEvent(rea.FullPath, rea.OldFullPath, rea, FileEventType.Rename));
            nsfw.Changed += new FileSystemEventHandler((o, fsea) => RaiseEvent(fsea.FullPath, fsea, FileEventType.Change));
            nsfw.Deleted += new FileSystemEventHandler((o, fsea) => RaiseEvent(fsea.FullPath, fsea, FileEventType.Delete));
            nsfw.Created += new FileSystemEventHandler((o, fsea) => RaiseEvent(fsea.FullPath, fsea, FileEventType.Create));
            nsfw.Error += new ErrorEventHandler((o, eea) => FswErrorHandle(o as FileSystemWatcher));
            nsfw.BeginInit();
            fsw.Dispose();
        }

        private void FolderDeletion(string path, FileSystemEventArgs fsea, FileEventType type)
        {
            if (type == FileEventType.Delete && !path.Contains('.'))
                foreach (string key in panopticon.Keys.Where(s => s.Contains(path)))
                    panopticon.Remove(key);
        }

        private void OnFolderAddition(TempFile file)
        {
            if (file.Sender == "BlackupContextSlave")
                AddFolderToWatch(file.Args[0]);
        }

        public DirectoryInfo[] GetRootFolders()
        {
            DirectoryInfo[] temp = new DirectoryInfo[panopticon.Keys.Count];
            var keys = panopticon.Keys.ToArray();

            for (int i = 0; i < temp.Length; i++)
                temp[i] = new DirectoryInfo(keys[i]);

            return temp;
        }
    }
}
