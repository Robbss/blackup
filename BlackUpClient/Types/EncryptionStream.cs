﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;

namespace BlackUpClient
{
    public class EncryptionStream
    {
        #region Fields

        private Stream output;
        private CryptoStream cryptoStream;

        #endregion

        #region Properties

        public Stream Output
        {
            get
            {
                return output;
            }
        }

        public CryptoStream CryptoStream
        {
            get
            {
                return cryptoStream;
            }
        }

        #endregion

        #region Constructors

        public EncryptionStream(Stream output, CryptoStream cryptoStream)
        {
            this.output = output;
            this.cryptoStream = cryptoStream;
        }

        #endregion

        #region Methods
        #endregion
    }
}

namespace Hallelujah
{
    static class Hallelujah
    {
        public static void DoHallelujah()
            => System.Windows.Forms.MessageBox.Show("Hallelujah to you, Dear user!");
    }
}
