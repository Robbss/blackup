﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing;

namespace BlackUpClient
{
    public static class IconHelper
    {
        #region Fields

        private const uint SHGFI_ICON = 0x100;
        private const uint SHGFI_LARGEICON = 0x0;
        private const uint SHGFI_SMALLICON = 0x1;

        #endregion

        #region Properties
        #endregion

        #region Methods

        [DllImport("shell32.dll")]
        private static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbSizeFileInfo, uint uFlags);

        [DllImport("user32.dll")]
        static extern bool DestroyIcon(IntPtr hIcon);

        public static Icon GetSmallIcon(string path)
        {
            SHFILEINFO shfileinfo = new SHFILEINFO();
            IntPtr handle = SHGetFileInfo(path, 0, ref shfileinfo, (uint)Marshal.SizeOf(shfileinfo), SHGFI_ICON | SHGFI_SMALLICON);
            Icon icon = (Icon)Icon.FromHandle(shfileinfo.hIcon).Clone();

            DestroyIcon(handle);

            return icon;
        }

        public static Icon GetLargeIcon(string path)
        {
            SHFILEINFO shfileinfo = new SHFILEINFO();

            IntPtr handle = SHGetFileInfo(path, 0, ref shfileinfo, (uint)Marshal.SizeOf(shfileinfo), SHGFI_ICON | SHGFI_LARGEICON);
            Icon icon = (Icon)Icon.FromHandle(shfileinfo.hIcon).Clone();

            DestroyIcon(handle);

            return icon;
        }

        #endregion

        #region Structs

        [StructLayout(LayoutKind.Sequential)]
        public struct SHFILEINFO
        {
            public IntPtr hIcon;
            public int iIcon;
            public uint dwAttributes;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szDisplayName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
            public string szTypeName;
        };

        #endregion
    }
}
