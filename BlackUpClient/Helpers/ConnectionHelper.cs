﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using BlackUpLib.Security;
using BlackUpLib.Netcode;

namespace BlackUpClient
{
    public static class ConnectionHelper
    {
        #region Fields

        private static Token token;

        private static string username;
        private static string password;

        #endregion

        #region Properties

        public static IPAddress IPAddress
        {
            get
            {
                return Dns.GetHostAddresses("robbss.net").First();
            }
        }

        public static Token Token
        {
            get
            {
                return token;
            }
        }

        public static bool IsTokenValid
        {
            get
            {
                return Token.Expires > DateTime.Now;
            }
        }

        #endregion

        #region Constructors
        #endregion

        #region Methods

        public static bool Login(string username, string password)
        {
            Client client = new Client(IPAddress, 25565);
            client.Connect();

            LoginPackage package = new LoginPackage(username, password);
            client.SendPackage(package);

            AuthorizationPackage author = client.ReadPackage<AuthorizationPackage>();

            if (author.AuthorizationState == AuthorizationState.Authorized)
            {
                token = author.AuthorizationToken;

                ConnectionHelper.username = username;
                ConnectionHelper.password = password;
            }

            return author.AuthorizationState == AuthorizationState.Authorized;
        }

        public static bool Relog()
        {
            return Login(username, password);
        }

        #endregion
    }
}
