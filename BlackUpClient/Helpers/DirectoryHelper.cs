﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace BlackUpClient
{
    public static class DirectoryHelper
    {
        #region Fields

        private static DirectoryContainer directoryContainer;

        #endregion

        #region Properties

        public static DirectoryContainer DirectoryContainer
        {
            get
            {
                return directoryContainer;
            }
            set
            {
                directoryContainer = value;
            }
        }

        public static List<string> Directories
        {
            get
            {
                return directoryContainer.Directories;
            }
        }

        #endregion

        #region Methods

        public static TreeNode CreateDirectoryNode(string path)
        {
            return CreateDirectoryNode(new DirectoryInfo(path));
        }

        private static TreeNode CreateDirectoryNode(DirectoryInfo directoryInfo)
        {
            TreeNode node = new TreeNode(directoryInfo.Name) { Tag = directoryInfo };

            foreach (string path in Directory.GetDirectories(directoryInfo.FullName))
            {
                try
                {
                    Directory.GetAccessControl(path);
                    node.Nodes.Add(CreateDirectoryNode(new DirectoryInfo(path)));
                }
                catch { }
            }

            return node;
        }

        public static void ReadDirectories(string path)
        {
            XmlSerializer serialer = new XmlSerializer(typeof(DirectoryContainer));

            if (File.Exists(path))
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    directoryContainer = (DirectoryContainer)serialer.Deserialize(reader);
                }
            }
            else
            {
                directoryContainer = new DirectoryContainer();
            }
        }

        public static void SaveDirectories(string path)
        {
            XmlSerializer serialer = new XmlSerializer(typeof(DirectoryContainer));

            using (StreamWriter writer = new StreamWriter(path))
            {
                serialer.Serialize(writer, directoryContainer);
            }
        }

        #endregion
    }
}
