﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BlackUpLib.Netcode;
using BlackUpLib.Security;
using System.Net;

namespace BlackUpClient
{
    public partial class CreateAccountForm : Form
    {
        public CreateAccountForm()
        {
            InitializeComponent();
        }

        private void CreateAccountForm_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Client client = new Client(ConnectionHelper.IPAddress, 25565);
            client.Connect();

            string username = textBox1.Text;
            string password = HashHelper.GenerateHash(textBox2.Text);

            CreateAccountPackage package = new CreateAccountPackage(username, password, textBox3.Text);
            client.SendPackage(package);

            AuthorizationPackage author = (AuthorizationPackage)client.ReadPackage();

            if (author.AuthorizationState == AuthorizationState.Authorized)
            {
                if (ConnectionHelper.Login(username, password))
                {
                    new RForm().Show();
                    Close();
                }
            }
            else
            {
                label4.Visible = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new LoginForm().Show();
            Close();
        }
    }
}
