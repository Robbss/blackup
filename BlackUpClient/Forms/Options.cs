﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;

namespace BlackUpClient
{
    public partial class Options : Form
    {
        private ConfigContainer config_container;
        private RForm parent;

        public Options(ConfigContainer config_container, RForm parent)
        {
            this.config_container = config_container;
            this.parent = parent;

            InitializeComponent();

            checkBox1.Checked = config_container.View.ShowExtensions;

            textBox2.Text = config_container.Connection.MaxUpload.ToString();
            textBox1.Text = config_container.Connection.MaxDownload.ToString();

            string hours = config_container.SyncTime.Hours.ToString();
            string minutes = config_container.SyncTime.Minutes.ToString();

            if (hours.Length == 1)
            {
                hours = "0" + hours;
            }

            if (minutes.Length == 1)
            {
                minutes = "0" + minutes;
            }

            domainUpDown1.Text = hours;
            domainUpDown2.Text = minutes;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Options_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConfigContainer.ViewConfigContainer viewConfig = new ConfigContainer.ViewConfigContainer(checkBox1.Checked);
            ConfigContainer.ConnectionConfigContainer connectionConfig = new ConfigContainer.ConnectionConfigContainer(int.Parse(textBox2.Text), int.Parse(textBox1.Text));

            TimeSpan syncTime = new TimeSpan(int.Parse(domainUpDown1.Text), int.Parse(domainUpDown2.Text), 0);

            ConfigContainer newconfig = new ConfigContainer() { View = viewConfig, Connection = connectionConfig, SyncTime = syncTime };

            using (StreamWriter writer = new StreamWriter("Config/Config.xml", false))
            {
                XmlSerializer serialer = new XmlSerializer(typeof(ConfigContainer));
                serialer.Serialize(writer, newconfig);
            }

            parent.Configure();
            this.Close();
        }

        private void domainUpDown_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
            return;
        }
    }
}
