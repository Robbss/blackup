﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BlackUpLib.Netcode;
using BlackUpLib.Security;
using System.Net;

namespace BlackUpClient
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text) || string.IsNullOrEmpty(textBox2.Text))
            {
                return;
            }

            string username = textBox1.Text;
            string password = HashHelper.GenerateHash(textBox2.Text);

            if (ConnectionHelper.Login(username, password))
            {
                RForm form = new RForm();
                Close();
                form.Show();
            }
            else
            {
                label3.Visible = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new CreateAccountForm().Show();
            Close();
        }
    }
}
