﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;
using BlackUpLib.Netcode;
using BlackUpLib.Environmenties;

namespace BlackUpClient
{
    public partial class RForm : Form
    {
        #region Fields

        private ConfigContainer config_container;
        private System.Threading.Timer timer;
        private Stage Stage;

        private const string DirectoryInfoPath = "Config/Directories.xml";

        private DirectoryInfo selected_directory;

        #endregion

        #region Properties
        #endregion

        #region Constructors

        public RForm()
        {
            InitializeComponent();

            DirectoryHelper.ReadDirectories(DirectoryInfoPath);
            foreach (string s in DirectoryHelper.Directories)
                FileWatcherManager.Instance.AddFolderToWatch(s);
        }

        #endregion

        #region Methods

        private void frmMain_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                mynotifyicon.Visible = true;
                mynotifyicon.ShowBalloonTip(500);
                this.mynotifyicon.Icon = new Icon("Icon/SafeBoxIcon.ico"); //The tray icon to use
                this.ShowInTaskbar = false;
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                mynotifyicon.Visible = false;
            }
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            mynotifyicon.Visible = false;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.WindowState = FormWindowState.Minimized;
            frmMain_Resize(this, e);
        }

        public void Configure()
        {
            XmlSerializer serialer = new XmlSerializer(typeof(ConfigContainer));

            using (StreamReader reader = new StreamReader("Config/Config.xml"))
            {
                config_container = (ConfigContainer)serialer.Deserialize(reader);
            }

            Thread thread = new Thread(PopulateTreeView);
            thread.Start();

            if (selected_directory != null)
            {
                PopulateListView();
            }

            StartTimer();
        }

        private void RForm_Load(object sender, EventArgs e)
        {
            label3.Visible = true;
            Configure();
            Stage = new Stage();
        }

        private void StartTimer()
        {
            DateTime currentTime = DateTime.Now;
            DateTime syncroTime = DateTime.Parse(config_container.SyncTime.ToString());

            TimeSpan wait = syncroTime.Subtract(currentTime);

            if (wait.TotalMilliseconds < 0)
            {
                wait = wait.Add(new TimeSpan(24, 0, 0));
            }

            timer = new System.Threading.Timer(Timer_Tick, null, (int)wait.TotalMilliseconds, Timeout.Infinite);
        }

        private void PopulateTreeView()
        {
            DirectoryHelper.ReadDirectories(DirectoryInfoPath);
            DirectoryHelper.SaveDirectories(DirectoryInfoPath);

            treeView1.Invoke((Action)delegate
            {
                treeView1.Nodes.Clear();
                label3.Visible = true;
            });

            List<Thread> threads = new List<Thread>();

            foreach (string directory in DirectoryHelper.Directories)
            {
                Thread thread = new Thread(() =>
                {
                    TreeNode root = DirectoryHelper.CreateDirectoryNode(directory);

                    treeView1.Invoke((Action)delegate
                    {
                        imageList1.Images.Add(IconHelper.GetSmallIcon(@"C:\Windows"));
                        treeView1.Nodes.Add(root);
                    });
                });

                threads.Add(thread);
                thread.Start();
            }

            foreach (Thread thread in threads)
            {
                thread.Join();
            }

            label3.Invoke((Action)delegate
            {
                label3.Visible = false;
            });
        }

        private void PopulateListView()
        {
            listView1.Clear();
            imageList2.Images.Clear();

            foreach (FileInfo fileInfo in selected_directory.GetFiles())
            {
                ListViewItem item = new ListViewItem();

                if (config_container.View.ShowExtensions)
                {
                    item.Text = Path.GetFileName(fileInfo.FullName);
                }
                else
                {
                    item.Text = Path.GetFileNameWithoutExtension(fileInfo.FullName);
                }

                item.ImageIndex = imageList2.Images.Count;
                item.Tag = fileInfo;

                imageList2.Images.Add(IconHelper.GetLargeIcon(fileInfo.FullName));
                listView1.Items.Add(item);
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            selected_directory = (DirectoryInfo)e.Node.Tag;
            PopulateListView();
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Options optionsForm = new Options(config_container, this);
            optionsForm.ShowDialog();
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            ListViewItem selected = listView1.SelectedItems[0];
            FileView fileView = new FileView((FileInfo)selected.Tag);

            fileView.ShowDialog();
        }

        private void fileSystemWatcher1_Renamed(object sender, RenamedEventArgs e)
        {
            MessageBox.Show($"Old: \"{e.OldName}\", New: \"{e.Name}\"");
        }


        private void listView1_KeyUp(object sender, KeyEventArgs e)
        {
            ContextMenu cm = new ContextMenu();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*
            this.mynotifyicon.BalloonTipText = "Whatever";
            this.mynotifyicon.BalloonTipTitle = "Title";
            this.mynotifyicon.Icon = new Icon("Icon/SafeBoxIcon.ico");
            this.mynotifyicon.Visible = true;
            this.mynotifyicon.ShowBalloonTip(30000);
            */

            DirectoryHelper.ReadDirectories(DirectoryInfoPath);

            List<string> dirs = DirectoryHelper.Directories;
            List<string> files = new List<string>();

            foreach (string directory in dirs)
            {
                files.AddRange(GetAllFiles(directory));
            }

            if (files.Count > 0)
            {
                SyncForm syncForm = new SyncForm(SyncMode.Upload, files.ToArray());
                syncForm.ShowDialog();
            }
        }

        private List<string> GetAllFiles(string directory)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(directory);
            List<string> result = new List<string>();

            foreach (FileInfo fileInfo in dirInfo.GetFiles("*", SearchOption.AllDirectories))
            {
                result.Add(fileInfo.FullName);
            }

            return result;
        }

        private void Timer_Tick(object state)
        {
            string[] s = (from c in Stage.FilesToSync() select c.Path).ToArray();

            if (s.Length > 0)
            {
                SyncForm sf = new SyncForm(SyncMode.Upload, s);
                sf.ShowDialog();
            }
        }

        private void backupNewFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "Select a folder to back up";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                BlackUpLib.Environmenties.FileWatcherManager.Instance.AddFolderToWatch(dialog.SelectedPath);
                DirectoryHelper.Directories.Add(dialog.SelectedPath);
                DirectoryHelper.SaveDirectories(DirectoryInfoPath);
                new Thread(() => StageAddition(dialog.SelectedPath)).Start();
            }

            Configure();
        }

        private void StageAddition(string path)
        {
            foreach (string s in Directory.GetFiles(path))
                Stage.Add(new Change(s));
            foreach (string s in Directory.GetDirectories(path))
                StageAddition(s);
        }

        #endregion
    }
}
