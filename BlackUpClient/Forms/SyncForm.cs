﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using BlackUpLib;
using BlackUpLib.Netcode;

namespace BlackUpClient
{
    public partial class SyncForm : Form
    {
        #region Fields

        private SyncMode syncMode;

        private string[] files;
        private int currentFile;

        private Client client;
        private ChunkSender chunkSender;

        private bool isPaused = false;
        private int currentIndex = 0;

        private const int chunkSize = 1024 * 1024 * 10;

        #endregion

        #region Properties
        #endregion

        #region Constructors

        public SyncForm(SyncMode syncMode, string[] files)
        {
            InitializeComponent();

            this.syncMode = syncMode;
            this.files = files;
        }

        #endregion

        #region Methods

        private void SyncForm_Load(object sender, EventArgs e)
        {
            int totalChunks = 0;

            foreach (string file in files)
            {
                totalChunks += ChunkCount(file);
            }

            progressBar2.Value = 0;
            progressBar2.Maximum = totalChunks;

            client = new Client(ConnectionHelper.IPAddress, 25565);
            client.Connect();

            Thread thread = new Thread(StartTransfer);
            thread.Start();
        }

        private void StartTransfer()
        {
            chunkSender = new ChunkSender(client, ConnectionHelper.Token, files[currentFile]);

            chunkSender.OnIndexChange += OnIndexChange;
            chunkSender.OnComplete += OnComplete;

            progressBar1.Invoke((Action)delegate
            {
                progressBar1.Value = 0;
                label4.Text = "0%";
            });

            int index = chunkSender.SendFile(chunkSize, currentIndex);

            if (index == -1)
            {
                currentIndex = 0;
                currentFile++;

                if (!(currentFile == files.Length))
                {
                    client.Reconnect();
                    StartTransfer();
                }
            }
            else
            {
                currentIndex = index;
            }
        }

        private void OnIndexChange(object sender, IndexChangeArgs e)
        {
            progressBar1.Invoke((Action)delegate
            {
                progressBar1.Value = (int)Math.Floor(((double)e.Index / (double)ChunkCount(files[currentFile])) * 100);
            });

            progressBar2.Invoke((Action)delegate
            {
                if (progressBar2.Value + 1 <= progressBar2.Maximum)
                {
                    progressBar2.Value += 1;
                }
            });

            label4.Invoke((Action)delegate
            {
                label4.Text = $"{Math.Round(((double)progressBar1.Value / (double)progressBar1.Maximum) * 100)}%";
            });

            label5.Invoke((Action)delegate
            {
                label5.Text = $"{Math.Round(((double)progressBar2.Value / (double)progressBar2.Maximum) * 100)}%";
            });

            if (isPaused)
            {
                chunkSender.Stop();
            }
        }

        private void OnComplete(object sender, EventArgs e)
        {
            if (currentFile + 1 == files.Length)
            {
                progressBar1.Invoke((Action)delegate
                {
                    Close();
                });
            }
        }

        private int ChunkCount(string file)
        {
            FileInfo fileInfo = new FileInfo(file);

            double totalCount = (double)fileInfo.Length / (double)chunkSize;
            int chunkCount = (int)Math.Ceiling(totalCount);

            return chunkCount;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button2.Enabled = true;

            isPaused = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button1.Enabled = true;
            button2.Enabled = false;

            isPaused = false;

            client.Reconnect();

            Thread thread = new Thread(StartTransfer);
            thread.Start();
        }

        #endregion
    }
}
