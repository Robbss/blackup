﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using BlackUpLib.Netcode;
using System.Net;
using BlackUpLib;

namespace BlackUpClient
{
    public partial class FileView : Form
    {
        private FileInfo info;

        private SortItem sortItem;
        private SortOrder sortOrder;

        public FileView(FileInfo info)
        {
            this.info = info;

            InitializeComponent();

            label1.Text = Path.GetFileName(info.FullName);
            pictureBox1.Image = IconHelper.GetLargeIcon(info.FullName).ToBitmap();

            if (!ConnectionHelper.IsTokenValid)
            {
                ConnectionHelper.Relog();
            }

            Client client = new Client(ConnectionHelper.IPAddress, 25565);
            client.Connect();

            client.SendPackage(new RequestDataPackage(ConnectionHelper.Token.Hash, RequestDataType.FileHistory, info.FullName));

            AuthorizationPackage author = client.ReadPackage<AuthorizationPackage>();

            if (author.AuthorizationState == AuthorizationState.Authorized)
            {
                FileHistoryPackage history = client.ReadPackage<FileHistoryPackage>();

                foreach (FileInformation fileInfo in history.FileInformation)
                {
                    listView1.Items.Add(CreateRow(fileInfo.Name, fileInfo.Date.ToString(), ""));
                }
            }

            SortView(SortItem.Date, SortOrder.Ascending);
        }

        private void SortView(SortItem sortItem, SortOrder sortOrder)
        {
            this.sortItem = sortItem;
            this.sortOrder = sortOrder;

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (ListViewItem item in listView1.Items)
            {
                items.Add(item);
            }

            listView1.Items.Clear();

            IEnumerable<ListViewItem> sorted = null;

            switch (sortItem)
            {
                case SortItem.Name:
                    sorted = items.OrderBy(o => o.Text);
                    break;

                case SortItem.Date:
                    sorted = items.OrderBy(o => Convert.ToDateTime(o.SubItems[1].Text));
                    break;

                case SortItem.Type:
                    sorted = items.OrderBy(o => o.SubItems[2].Text);
                    break;
            }

            if (sortOrder == SortOrder.Descending)
            {
                sorted = sorted.Reverse();
            }

            listView1.SetSortIcon((int)sortItem, sortOrder);

            foreach (ListViewItem item in sorted)
            {
                listView1.Items.Add(item);
            }
        }

        private ListViewItem CreateRow(string name, string date, string type)
        {
            ListViewItem item = new ListViewItem(name);

            item.SubItems.Add(new ListViewItem.ListViewSubItem(item, date));
            item.SubItems.Add(new ListViewItem.ListViewSubItem(item, type));

            return item;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            SortItem sortItem = (SortItem)e.Column;
            SortOrder sortOrder = SortOrder.Ascending;

            if (sortItem == this.sortItem && this.sortOrder == SortOrder.Ascending)
            {
                sortOrder = SortOrder.Descending;
            }

            SortView(sortItem, sortOrder);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                button2.Enabled = false;
            }
            else
            {
                button2.Enabled = true;
            }
        }
    }
}
