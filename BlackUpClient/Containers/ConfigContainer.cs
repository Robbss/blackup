﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BlackUpClient
{
    public struct ConfigContainer
    {
        #region Fields

        private ViewConfigContainer view;
        private ConnectionConfigContainer connection;
        private TimeSpan syncTime;

        #endregion

        #region Properties

        public ViewConfigContainer View
        {
            get
            {
                return view;
            }
            set
            {
                view = value;
            }
        }

        public ConnectionConfigContainer Connection
        {
            get
            {
                return connection;
            }
            set
            {
                connection = value;
            }
        }

        [XmlIgnore]
        public TimeSpan SyncTime
        {
            get
            {
                return syncTime;
            }
            set
            {
                syncTime = value;
            }
        }

        [XmlElement("SyncTime")]
        public long SyncTimeTicks
        {
            get
            {
                return syncTime.Ticks;
            }
            set
            {
                syncTime = new TimeSpan(value);
            }
        }

        #endregion

        #region Constructors

        #endregion

        #region Methods
        #endregion

        #region Structs

        [XmlRoot("View")]
        public struct ViewConfigContainer
        {
            #region Fields

            private bool showExtensions;

            #endregion

            #region Properties

            public bool ShowExtensions
            {
                get
                {
                    return showExtensions;
                }
                set
                {
                    showExtensions = value;
                }
            }

            #endregion

            #region Constructors

            public ViewConfigContainer(bool showExtensions = false)
            {
                this.showExtensions = showExtensions;
            }

            #endregion

            #region Methods
            #endregion
        }

        [XmlRoot("Connection")]
        public struct ConnectionConfigContainer
        {
            #region Fields

            private int maxUpload;
            private int maxDownload;

            #endregion

            #region Properties

            public int MaxUpload
            {
                get
                {
                    return maxUpload;
                }
                set
                {
                    maxUpload = value;
                }
            }

            public int MaxDownload
            {
                get
                {
                    return maxDownload;
                }
                set
                {
                    maxDownload = value;
                }
            }

            #endregion

            #region Constructors

            public ConnectionConfigContainer(int maxUpload = 10, int maxDownload = 10)
            {
                this.maxUpload = maxUpload;
                this.maxDownload = maxDownload;
            }

            #endregion
        }

        #endregion
    }
}
