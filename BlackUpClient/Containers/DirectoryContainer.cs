﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BlackUpClient
{
    public class DirectoryContainer
    {
        #region Fields

        private List<string> directories;

        #endregion

        #region Properties

        [XmlArray]
        public List<string> Directories
        {
            get
            {
                return directories;
            }
            set
            {
                directories = value;
            }
        }

        #endregion

        #region Constructors

        public DirectoryContainer()
        {
            directories = new List<string>();
        }

        #endregion

        #region Methods
        #endregion
    }
}
