﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackUpLib.Netcode;
using BlackUpLib;
using System.Net.Sockets;
using MySql.Data.MySqlClient;

namespace BlackUpServer
{
    public class CreateAccountHandler : IMessageHandler
    {
        #region Fields

        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Methods

        public void Handle(Socket socket, Package package)
        {
            CreateAccountPackage accountPackage = (CreateAccountPackage)package;

            MySqlDataReader reader = Program.DBHelper.Select("*", "accounts", $"username='{accountPackage.Username}'");

            if (!reader.Read())
            {
                reader.Close();

                Program.DBHelper.Insert("accounts", "username, password, email, guid", $"'{accountPackage.Username}', '{accountPackage.Password}', '{accountPackage.Email}', '{Guid.NewGuid().ToString("B")}'");
                Program.DisplayHelper.WriteLog($"Created account.. [{accountPackage.Username}]");

                Connection.SendPackage(socket, new AuthorizationPackage(AuthorizationState.Authorized, null));
            }
            else
            {
                reader.Close();
                Connection.SendPackage(socket, new AuthorizationPackage(AuthorizationState.Denied, null));
            }

            socket.Close();
        }

        #endregion
    }
}
