﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using BlackUpLib.Netcode;
using BlackUpLib;
using MySql.Data.MySqlClient;

namespace BlackUpServer
{
    public class LoginHandler : IMessageHandler
    {
        #region Fields

        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Methods

        public void Handle(Socket socket, Package package)
        {
            LoginPackage loginPackage = (LoginPackage)package;

            MySqlDataReader reader = Program.DBHelper.Select("guid", "accounts", $"username='{loginPackage.Username}' and password='{loginPackage.Hash}'");

            if (reader.Read())
            {
                Token token = Token.Create(loginPackage.Username, 120);

                string guid = reader.GetString(0);
                reader.Close();

                Program.Tokens.Add(token, guid);

                Connection.SendPackage(socket, new AuthorizationPackage(AuthorizationState.Authorized, token));
                Program.DisplayHelper.WriteLog($"Login successful.. [{loginPackage.Username}]");
            }
            else
            {
                reader.Close();
                Connection.SendPackage(socket, new AuthorizationPackage(AuthorizationState.Denied, null));
                Program.DisplayHelper.WriteLog($"Login Failed.. [{loginPackage.Username}]");
            }

            socket.Close();
        }

        #endregion
    }
}
