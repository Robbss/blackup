﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackUpLib.Netcode;
using BlackUpLib;
using System.Net.Sockets;
using System.IO;

namespace BlackUpServer
{
    public class ChunkHandler : IMessageHandler
    {
        #region Fields

        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Methods

        public void Handle(Socket socket, Package package)
        {
            ChunkStartPackage chunkPackage = (ChunkStartPackage)package;

            if (!TokenHelper.IsValid(chunkPackage.Token))
            {
                Connection.SendPackage(socket, new AuthorizationPackage(AuthorizationState.Expired, null));
                return;
            }

            string guid = TokenHelper.GetGUID(chunkPackage.Token);
            int currentChunk = chunkPackage.ChunkCurrent;

            DirectoryInfo directoryInfo = new DirectoryInfo(StoreHelper.GetDownloadPath(guid, chunkPackage.FileName));

            if (chunkPackage.ChunkCurrent > 0)
            {
                if (directoryInfo.Exists)
                {
                    for (int i = 0; i < chunkPackage.ChunkCurrent - 1; i++)
                    {
                        if (!directoryInfo.EnumerateFiles($"*.chunk{i}", SearchOption.TopDirectoryOnly).Any())
                        {
                            Connection.SendPackage(socket, new AuthorizationPackage(AuthorizationState.Denied, null));
                            return;
                        }
                    }
                }
                else
                {
                    Connection.SendPackage(socket, new AuthorizationPackage(AuthorizationState.Denied, null));
                    return;
                }
            }

            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }

            Connection.SendPackage(socket, new AuthorizationPackage(AuthorizationState.Authorized, null));

            if (chunkPackage.ChunkCount == 1)
            {
                object raw = Connection.ReadPackage(socket);
                ChunkDataPackage chunkData = (ChunkDataPackage)raw;

                string filepath = Path.Combine(directoryInfo.FullName, $"{chunkPackage.FileName}.chunk{chunkData.ChunkIndex}");

                using (FileStream fs = new FileStream(filepath, FileMode.Create))
                {
                    fs.Write(chunkData.ChunkData, 0, chunkData.ChunkData.Length);
                    fs.Flush();
                }
            }
            else
            {
                while (currentChunk < chunkPackage.ChunkCount)
                {
                    Package raw = Connection.ReadPackage(socket);
                    ChunkDataPackage chunkData = null;

                    if (raw is ChunkDataPackage)
                    {
                        chunkData = (ChunkDataPackage)raw;
                    }
                    else
                    {
                        return;
                    }

                    string filepath = Path.Combine(directoryInfo.FullName, $"{chunkPackage.FileName}.chunk{chunkData.ChunkIndex}");

                    using (FileStream fs = new FileStream(filepath, FileMode.Create))
                    {
                        fs.Write(chunkData.ChunkData, 0, chunkData.ChunkData.Length);
                        fs.Flush();
                    }

                    currentChunk = chunkData.ChunkIndex + 1;
                }
            }

            string combinePath = Path.Combine(StoreHelper.GetDownloadPath(guid, chunkPackage.FileName), $"{chunkPackage.FileName}.{DateTime.Now.ToString("ddMMyyhhmmss")}");

            using (FileStream combineStream = new FileStream(combinePath, FileMode.Create))
            {
                FileInfo[] files = directoryInfo.GetFiles("*.chunk*");

                foreach (FileInfo fileInfo in files.OrderBy(o => o.LastWriteTime))
                {
                    using (FileStream partStream = new FileStream(fileInfo.FullName, FileMode.Open))
                    {
                        byte[] buffer = new byte[partStream.Length];

                        partStream.Read(buffer, 0, buffer.Length);
                        combineStream.Write(buffer, 0, buffer.Length);
                    }

                    File.Delete(fileInfo.FullName);
                }
            }

            FileInfo finalInfo = new FileInfo(combinePath);

            string fileguid = StoreHelper.CreateFilePath(guid, chunkPackage.FilePath);
            string filestorepath = Path.Combine(StoreHelper.GetFilePath(guid, fileguid), finalInfo.Name);

            try
            {
                finalInfo.MoveTo(filestorepath);
            }
            catch (Exception ex)
            {
                Program.DisplayHelper.WriteLog($"Could not copy file.. {ex.Message}");
            }
            finally
            {
                socket.Close();
            }
        }

        #endregion
    }
}
