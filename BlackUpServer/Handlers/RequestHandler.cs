﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using BlackUpLib;
using BlackUpLib.Netcode;

namespace BlackUpServer
{
    public class RequestHandler : IMessageHandler
    {
        #region Fields

        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Methods

        public void Handle(Socket socket, Package package)
        {
            RequestDataPackage request = (RequestDataPackage)package;

            switch (request.RequestDataType)
            {
                case RequestDataType.FileHistory:
                    HandleFileHistory(socket, request);
                    break;
            }

            socket.Close();
        }

        private void HandleFileHistory(Socket socket, RequestDataPackage package)
        {
            if (!TokenHelper.IsValid(package.Token))
            {
                Connection.SendPackage(socket, new AuthorizationPackage(AuthorizationState.Expired, null));
                return;
            }
            else
            {
                Connection.SendPackage(socket, new AuthorizationPackage(AuthorizationState.Authorized, null));
            }

            string guid = TokenHelper.GetGUID(package.Token);

            string filePath = package.RequestInformation[0];
            string fileGuid = StoreHelper.FindFileGUID(guid, filePath);

            string storePath = StoreHelper.GetFilePath(guid, fileGuid);

            DirectoryInfo directoryInfo = new DirectoryInfo(storePath);
            FileHistoryPackage result = new FileHistoryPackage();

            if (directoryInfo.Exists)
            {
                foreach (FileInfo file in directoryInfo.GetFiles())
                {
                    if (file.Extension == ".manifest")
                    {
                        continue;
                    }

                    result.FileInformation.Add(new FileInformation(file.Name));
                }
            }

            Connection.SendPackage(socket, result);
        }

        #endregion
    }
}
