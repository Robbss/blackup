﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace BlackUpServer
{
    public partial class DBHelperMySQL
    {
        #region Fields

        private MySqlConnection connection;

        protected const string selectSerial = "select {0} from {1} where {2};";
        protected const string insertSerial = "insert into {0} ({1}) values ({2});";
        protected const string updateSerial = "update {0} set {1} where {2};";

        #endregion

        #region Properties

        public MySqlConnection Connection
        {
            get
            {
                return connection;
            }
        }

        #endregion

        #region Constructors

        public DBHelperMySQL()
        {
            connection = new MySqlConnection();
        }

        #endregion

        #region Methods

        public void Connect(string url, string database, string username, string password)
        {
            connection = new MySqlConnection($"Server={url};Uid={username};Pwd={password};Database={database};");
            connection.Open();
        }

        public MySqlDataReader Select(string columns, string table, string filter)
        {
            MySqlCommand command = new MySqlCommand(string.Format(selectSerial, columns, table, filter), connection);
            return command.ExecuteReader();
        }

        public void Insert(string table, string columns, string values)
        {
            MySqlCommand command = new MySqlCommand(string.Format(insertSerial, table, columns, values), connection);
            command.ExecuteNonQuery();
        }

        public void Update(string table, string values, string filter)
        {
            MySqlCommand command = new MySqlCommand(string.Format(updateSerial, table, values, filter), connection);
            command.ExecuteNonQuery();
        }

        #endregion
    }
}
