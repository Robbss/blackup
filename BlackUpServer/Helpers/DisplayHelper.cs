﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackUpServer
{
    public class DisplayHelper
    {
        #region Fields

        private List<string> log;
        private int logRow;
        private int logMax;

        #endregion

        #region Properties
        #endregion

        #region Constructors

        public DisplayHelper(int logRow, int logMax)
        {
            this.log = new List<string>();

            this.logRow = logRow;
            this.logMax = logMax;
        }

        #endregion

        #region Methods

        public void WriteLog(string message)
        {
            string combined = $" [{DateTime.Now.ToString("dd-MM hh:mm:ss")}] {message}";

            log.Add(combined);

            if (log.Count > logMax)
            {
                log.RemoveAt(0);
            }

            RedrawLog();
        }

        public void Write(int row, string message)
        {
            Console.SetCursorPosition(0, row);
            Console.Write(new string(' ', Console.BufferWidth));

            Console.SetCursorPosition(0, row);
            Console.Write(message);
        }

        public void RedrawLog()
        {
            for (int i = 0; i < log.Count; i++)
            {
                Console.SetCursorPosition(0, i + logRow);
                Console.Write(new string(' ', Console.BufferWidth));

                Console.SetCursorPosition(0, i + logRow);
                Console.Write(log[i]);
            }
        }

        #endregion
    }
}
