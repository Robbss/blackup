﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace BlackUpServer
{
    public static class StoreHelper
    {
        #region Fields

        private static string root;
        private static Dictionary<string, Dictionary<string, string>> fileGUIDs;

        private static object syncro;

        #endregion

        #region Properties

        public static string Root
        {
            get
            {
                return root;
            }
            set
            {
                root = value;
            }
        }

        public static Dictionary<string, Dictionary<string, string>> FileGUIDs
        {
            get
            {
                return fileGUIDs;
            }
        }

        private static object Syncro
        {
            get
            {
                if (syncro == null)
                {
                    syncro = new object();
                }

                return syncro;
            }
        }

        #endregion

        #region Methods

        public static void FillFileGUIDs()
        {
            DirectoryInfo rootInfo = new DirectoryInfo(root);
            XmlSerializer serializer = new XmlSerializer(typeof(Manifest));

            fileGUIDs = new Dictionary<string, Dictionary<string, string>>();

            foreach (DirectoryInfo directoryInfo in rootInfo.GetDirectories())
            {
                Dictionary<string, string> files = new Dictionary<string, string>();

                foreach (DirectoryInfo subInfo in directoryInfo.GetDirectories())
                {
                    if (subInfo.Name == "download")
                    {
                        continue;
                    }

                    string manifestPath = Path.Combine(subInfo.FullName, "file.manifest");

                    if (File.Exists(manifestPath))
                    {
                        using (FileStream fs = File.Open(manifestPath, FileMode.Open))
                        {
                            Manifest manifest = (Manifest)serializer.Deserialize(fs);
                            files.Add(manifest.Path, subInfo.Name);
                        }
                    }
                }

                fileGUIDs.Add(directoryInfo.Name, files);
            }
        }

        public static Dictionary<string, string> GetAllFileGUIDs(string guid)
        {
            Dictionary<string, string> results = new Dictionary<string, string>();

            DirectoryInfo directoryInfo = new DirectoryInfo(Path.Combine(root, guid));
            XmlSerializer serializer = new XmlSerializer(typeof(Manifest));

            lock (Syncro)
            {
                if (directoryInfo.Exists)
                {
                    foreach (DirectoryInfo subInfo in directoryInfo.GetDirectories())
                    {
                        if (subInfo.Name == "download")
                        {
                            continue;
                        }

                        string manifestPath = Path.Combine(subInfo.FullName, "file.manifest");

                        if (File.Exists(manifestPath))
                        {
                            using (FileStream fs = File.Open(manifestPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                            {
                                Manifest manifest = (Manifest)serializer.Deserialize(fs);
                                results.Add(manifest.Path, subInfo.Name);
                            }
                        }
                    }
                }
            }

            return results;
        }

        public static string FindFileGUID(string guid, string path)
        {
            Dictionary<string, string> results = GetAllFileGUIDs(guid);

            if (results.ContainsKey(path))
            {
                return results[path];
            }

            return Guid.NewGuid().ToString("B");
        }

        public static string CreateFilePath(string guid, string path)
        {
            string fileguid = FindFileGUID(guid, path);
            string location = GetFilePath(guid, fileguid);
            string manifest = Path.Combine(location, "file.manifest");

            Directory.CreateDirectory(location);

            lock (Syncro)
            {
                using (FileStream fs = new FileStream(manifest, FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Manifest));
                    serializer.Serialize(fs, new Manifest(path));
                }
            }

            return fileguid;
        }

        public static string GetFilePath(string guid, string fileguid)
        {
            return Path.Combine(root, guid, fileguid);
        }

        public static string GetDownloadPath(string guid, string id)
        {
            return Path.Combine(root, guid, "download", id);
        }

        #endregion
    }
}
