﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace BlackUpServer
{
    public partial class DBHelperMySQL
    {
        #region Methods

        public string GetUserGUID(string username)
        {
            MySqlDataReader reader = Select("guid", "accounts", $"username={username}");

            if (reader.Read())
            {
                return reader.GetString(0);
            }

            return "";
        }

        #endregion
    }
}
