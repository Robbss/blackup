﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackUpLib.Netcode;

namespace BlackUpServer
{
    public static class TokenHelper
    {
        #region Fields        
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Methods

        public static bool IsValid(string tokenHash)
        {
            KeyValuePair<Token, string> pair = Program.Tokens.FirstOrDefault(o => o.Key.Hash == tokenHash);

            if (pair.Equals(default(KeyValuePair<Token, string>)))
            {
                return false;
            }
            else
            {
                if (pair.Key.Expires > DateTime.Now)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static string GetGUID(string tokenHash)
        {
            return Program.Tokens.First(o => o.Key.Hash == tokenHash).Value;
        }

        #endregion
    }
}
