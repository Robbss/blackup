﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackUpServer
{
    public class Manifest
    {
        #region Fields

        private string path;

        #endregion

        #region Properties

        public string Path
        {
            get
            {
                return path;
            }

            set
            {
                path = value;
            }
        }

        #endregion

        #region Constructors

        public Manifest()
        {
            this.path = "";
        }

        public Manifest(string path)
        {
            this.path = path;
        }

        #endregion

        #region Methods
        #endregion
    }
}
