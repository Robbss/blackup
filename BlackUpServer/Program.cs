﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Net;
using BlackUpLib.Netcode;
using BlackUpLib.Security;
using System.Diagnostics;

namespace BlackUpServer
{
    public class Program
    {
        #region Fields

        private static Server server;
        private static DBHelperMySQL dbHelper;
        private static DisplayHelper displayHelper;
        private static DateTime startTime;
        private static Dictionary<Token, string> tokens;

        private static Timer timer;
        private static Timer timer2;

        #endregion

        #region Properties

        public static DBHelperMySQL DBHelper
        {
            get
            {
                return dbHelper;
            }
            set
            {
                dbHelper = value;
            }
        }

        public static DisplayHelper DisplayHelper
        {
            get
            {
                return displayHelper;
            }
        }

        public static Dictionary<Token, string> Tokens
        {
            get
            {
                return tokens;
            }
        }

        #endregion

        #region Methods

        static void Main(string[] args)
        {
            Console.Title = "BlackUp - Server";
            //Console.BufferHeight = 25;

            tokens = new Dictionary<Token, string>();

            server = new Server(25565);
            dbHelper = new DBHelperMySQL();

            server.Register(typeof(LoginPackage), new LoginHandler());
            server.Register(typeof(ChunkStartPackage), new ChunkHandler());
            server.Register(typeof(CreateAccountPackage), new CreateAccountHandler());
            server.Register(typeof(RequestDataPackage), new RequestHandler());

            startTime = DateTime.Now;

            StoreHelper.Root = "root";
            displayHelper = new DisplayHelper(15, 9);

            DrawUI();

            timer = new Timer((o) => { UpdateUI(); }, null, 500, 500);
            timer2 = new Timer((o) => { UpdateTokens(); }, null, 5000, 5000);

            Thread thread = server.Start();

            displayHelper.WriteLog("Started server..");

            dbHelper.Connect("robbss.net", "blackup", "dbuser", "craplagg");

            displayHelper.WriteLog("Connected to database..");

            thread.Join();
        }

        static void DrawUI()
        {
            displayHelper.Write(0, new string('#', Console.BufferWidth));
            displayHelper.Write(1, $"####{new string(' ', 29)}BlackUp Server{new string(' ', 29)}####");
            displayHelper.Write(2, new string('#', Console.BufferWidth));

            displayHelper.Write(13, "  Server log");
            displayHelper.Write(14, new string('-', Console.BufferWidth));

            UpdateUI();
        }

        static void UpdateUI()
        {
            ProcessThreadCollection currentThreads = Process.GetCurrentProcess().Threads;

            displayHelper.Write(4, $"  Uptime.........{(DateTime.Now - startTime).ToString(@"hh\:mm\:ss")}");
            displayHelper.Write(6, $"  Database.......{dbHelper.Connection.State}");
            displayHelper.Write(8, $"  Tokens.........{tokens.Count}");
            displayHelper.Write(10, $"  Threads........{currentThreads.Count}");
        }

        static void UpdateTokens()
        {
            foreach (Token token in tokens.Keys.ToArray())
            {
                if (token.Expires < DateTime.Now)
                {
                    tokens.Remove(token);
                }
            }
        }

        #endregion
    }
}
