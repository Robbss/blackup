﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BlackUpLib.Netcode;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace BlackUpLibTest.Netcode.Packages
{
    [TestClass]
    public class PackageTest
    {
        [TestMethod]
        public void TestPackageSerialization()
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();

            Package package = new Package("token1-2-3");

            MemoryStream ms = new MemoryStream();

            binaryFormatter.Serialize(ms, package);

            ms.Position = 0;

            Package package2 = (Package)binaryFormatter.Deserialize(ms);

            Assert.AreEqual(package, package2);
        }

        [TestMethod]
        public void TestLoginPackageSerialization()
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();

            LoginPackage package = new LoginPackage("username", "hash");

            MemoryStream ms = new MemoryStream();

            binaryFormatter.Serialize(ms, package);

            ms.Position = 0;

            LoginPackage package2 = (LoginPackage)binaryFormatter.Deserialize(ms);

            Assert.AreEqual(package, package2);
        }

        [TestMethod]
        public void TestChunkDataPackageSerialization()
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();

            ChunkDataPackage package = new ChunkDataPackage("token-1", 0, new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 });

            MemoryStream ms = new MemoryStream();

            binaryFormatter.Serialize(ms, package);

            ms.Position = 0;

            ChunkDataPackage package2 = (ChunkDataPackage)binaryFormatter.Deserialize(ms);

            Assert.AreEqual(package, package2);
        }
    }
}
