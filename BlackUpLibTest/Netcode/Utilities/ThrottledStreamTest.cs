﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BlackUpLib.Netcode;
using System.IO;

namespace BlackUpLibTest.Netcode.Utilities
{
    [TestClass]
    public class ThrottledStreamTest
    {
        [TestMethod]
        public void TestThrottledStreamWrite()
        {
            byte[] payload = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            MemoryStream ms = new MemoryStream();
            ThrottledStream throttledStream = new ThrottledStream(ms);

            throttledStream.Write(payload, 0, payload.Length);
            ms.Position = 0;

            byte[] received = new byte[payload.Length];
            ms.Read(received, 0, payload.Length);

            for (int i = 0; i < received.Length; i++)
            {
                Assert.AreEqual(payload[i], received[i]);
            }
        }

        [TestMethod]
        public void TestThrottledStreamRead()
        {
            byte[] payload = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            MemoryStream ms = new MemoryStream(payload);
            ThrottledStream throttledStream = new ThrottledStream(ms);

            byte[] received = new byte[payload.Length];
            throttledStream.Read(received, 0, payload.Length);

            for (int i = 0; i < received.Length; i++)
            {
                Assert.AreEqual(payload[i], received[i]);
            }
        }

    }
}
