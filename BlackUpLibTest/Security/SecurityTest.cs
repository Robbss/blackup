﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BlackUpLib.Netcode;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace BlackUpLibTest.Security
{
    [TestClass]
    public class SecurityTest
    {
        [TestMethod]
        public void TestEncryption()
        {
            string s = "Hello World", s2;
            MemoryStream ms = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(s));
            BlackUpLib.Security.EncryptionHelper eh = new BlackUpLib.Security.EncryptionHelper();
            ms.Position = 0;

            using (StreamReader sr = new StreamReader(eh.EncryptStream(ms, "yum")))
            {
                s2 = sr.ReadToEnd();
            }

            Assert.AreNotEqual(s, s2);
        }

        [TestMethod]
        public void TestDecryption()
        {
            string s = "Hello World", s2;
            MemoryStream ms = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(s));
            BlackUpLib.Security.EncryptionHelper eh = new BlackUpLib.Security.EncryptionHelper();
            ms.Position = 0;

            using (StreamReader sr = new StreamReader(eh.DecryptStream(eh.EncryptStream(ms, "yum"), "yum")))
            {
                s2 = sr.ReadToEnd().TrimEnd('\0');
            }

            Assert.AreEqual(s, s2);
        }

        [TestMethod]
        public void TestHash()
        {
            string s = "Hello World", s2 = BlackUpLib.Security.HashHelper.GenerateHash(s);
            Assert.AreNotEqual(s, s2);
        }
    }
}
